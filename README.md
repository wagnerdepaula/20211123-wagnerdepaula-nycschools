# 20211123-WagnerDePaula-NYCSchools

## Description
A native app to provide information on NYC High schools.

## Hierarchy
This project has been broken into four main screens:
<br><br>

1. **ListViewController**
    1. That's where all NYC High Schools get pulled and listed from the City Of New York Database.<br><br>

2. **MapViewController**
    1. Similar to ListViewController, this is a mapView where all NYC High Schools get pulled and then superimposed on top of a New York City map.<br><br>

3. **SchoolViewController**
    1. This view controller displays the following detailed information on each selection High School.
        1. **Header**
            - School name, total students, grades and attendance rate
        2. **Contact**
            - Call to actions to phone call, email, navigate to website or share school.
        3. **Address**
            - Street, zip, borough and city
        4. **Transportation**
            - Subway and buses lines
        5. **Overview**
            - Detailed summary about the school
        6. **SAT scores**
            - Math, reading and writing scores
        7. **Activities**
            - Extra curricular or sports activities
<br><br>
3. **SettingsViewController**
    1. Ability to switch between Dark and Light modes<br><br>
    

## Visuals
Below a few screenshots on this project:

<video width="280" height="606" style="width:280px;height:606px;border:1px solid #333333" playsinline autoplay muted loop><source src="video/20211123-wagnerdepaula-nycschools.mp4" type="video/mp4"><source src="video/20211123-wagnerdepaula-nycschools.mov" type="video/mov"></video>
<img src="images/list.png" width="280">
<img src="images/details.png" width="280">
<img src="images/map.png" width="280">
<img src="images/sat.png" width="280">
<img src="images/email.png" width="280">
<img src="images/share.png" width="280">
<img src="images/call.png" width="280">
