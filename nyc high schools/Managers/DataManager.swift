//
//  DataManager.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit

public class DataManager: NSObject {
    
    static var shared: DataManager = DataManager()
    
    public var currentSchool: SchoolItem = SchoolItem()
    public var currentSat: SatItem = SatItem()
    public var schoolItems: [SchoolItem] = []
    public var satItems: [SatItem] = []
    
    public lazy var session: URLSession = {
        return URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
    }()
    
    public func getSchools(completion: @escaping () -> ()) {
        
        let urlString: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        guard let url: URL = URL(string: urlString) else { return }
        let urlRequest: URLRequest = URLRequest(url: url)
        
        session.dataTask(with: urlRequest) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else { return }
                do {
                    let schoolItems = try JSONDecoder().decode([SchoolItem].self, from: data)
                    self.schoolItems = schoolItems
                    completion()
                } catch {
                    print("Failed to decode", error)
                    return
                }
            }
            
        }.resume()
    }
    
    
    public func getSatScores(completion: @escaping () -> ()) {
        
        // Resets current SAT Scores
        self.currentSat = SatItem()
        
        let urlString: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        guard let url: URL = URL(string: urlString) else { return }
        let urlRequest: URLRequest = URLRequest(url: url)
        
        session.dataTask(with: urlRequest) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else { return }
                do {
                    let satItems = try JSONDecoder().decode([SatItem].self, from: data)
                    self.satItems = satItems
                    self.setCurrentSatScore {
                        completion()
                    }
                } catch {
                    print("Failed to decode", error)
                    return
                }
            }
            
        }.resume()
    }
    
    
    public func setCurrentSchool(with schoolItem: SchoolItem) {
        currentSchool = schoolItem
    }
    
    public func setCurrentSatScore(completion: @escaping () -> ()) {
        guard let dbn = currentSchool.dbn else { return }
        guard let item = satItems.filter({$0.dbn == dbn}).first else { return }
        self.currentSat = item
        completion()
    }
    
}
