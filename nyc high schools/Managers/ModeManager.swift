//
//  ModeManager.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit


final class ModeManager: NSObject {
    
    static var shared: ModeManager = ModeManager()
    
    public func set(mode: Int?) {

        let userInterfaceStyle: UIUserInterfaceStyle
        
        switch mode {
        case 1:
            userInterfaceStyle = .light
        case 2:
            userInterfaceStyle = .dark
        default:
            userInterfaceStyle = .unspecified
        }
        
        guard let window = UIApplication.shared.windows.first else { return }
        window.overrideUserInterfaceStyle = userInterfaceStyle
        
    }
    
    
    public func get() -> UIUserInterfaceStyle {
        guard let window = UIApplication.shared.windows.first else { return .unspecified }
        return window.overrideUserInterfaceStyle
    }
    
}
