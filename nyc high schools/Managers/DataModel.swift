//
//  DataModel.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import Foundation


public struct SchoolItem: Codable {
    
    var dbn: String?
    var schoolName: String?
    var overviewParagraph: String?
    var academicOpportunities1: String?
    var academicOpportunities2: String?
    
    var location: String?
    var phoneNumber: String?
    var faxNumber: String?
    var email: String?
    var website: String?
    var subway: String?
    var bus: String?
    
    var grades2018: String?
    var finalGrades: String?
    var totalStudents: String?
    var extracurricularActivities: String?
    var schoolSports: String?
    var attendanceRate: String?
    
    var address: String?
    var city: String?
    var zip: String?
    var stateCode: String?
    var latitude: String?
    var longitude: String?
    var borough: String?
    
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        
        case location = "location"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case email = "school_email"
        case website = "website"
        case subway = "subway"
        case bus = "bus"
        
        case grades2018 = "grades2018"
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        
        case address = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case stateCode = "state_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case borough = "borough"
        
    }
    
}



public struct SatItem: Codable {
    
    var dbn: String?
    var schoolName: String?
    var numberOfTests: String?
    var readingScore: String?
    var mathScore: String?
    var writingScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numberOfTests = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
       
}


