//
//  MapViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit
import MapKit

final class MapViewController: UIViewController, MKMapViewDelegate {
    
    public var schoolItems: [SchoolItem] = []
    
    // A subtle impact feedback is always good and gives a more intentional experience
    public lazy var generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .light)
        return generator
    }()
    
    private lazy var mapView: MKMapView = {
        let mapView: MKMapView = MKMapView(frame: .zero)
        let center = CLLocationCoordinate2D(latitude: 40.7128, longitude: -74.0060)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.setRegion(region, animated: true)
        mapView.delegate = self
        return mapView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigation()
        getData()
    }
    
    private func setupView() {
        view.backgroundColor = .systemBackground
        view.addSubview(mapView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
    }
    
    private func updateNavigation() {
        navigationItem.title = "Map"
    }
    
    private func getData() {
        DataManager.shared.getSchools {
            self.schoolItems = DataManager.shared.schoolItems
            self.setupLocationMark()
        }
    }
    
    private func setupLocationMark() {
        
        for item in self.schoolItems {
            
            guard let schoolName = item.schoolName else { return }
            guard let latitude = Double(item.latitude ?? "0") else { return }
            guard let longitude = Double(item.longitude ?? "0")  else { return }
            
            let pin = Pin(title: schoolName, coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude), schoolItem: item)
            mapView.addAnnotation(pin)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? Pin {
            generator.impactOccurred()
            setCurrentSchool(schoolItem: annotation.schoolItem)
        }
    }
    
    private func setCurrentSchool(schoolItem: SchoolItem) {
        DataManager.shared.setCurrentSchool(with: schoolItem)
        NavigationController.shared.navigateToSchool()
    }
    
}


class Pin: NSObject, MKAnnotation {
    
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var schoolItem: SchoolItem
    
    init(title: String, coordinate: CLLocationCoordinate2D, schoolItem: SchoolItem  ) {
        self.title = title
        self.coordinate = coordinate
        self.schoolItem = schoolItem
    }
}
