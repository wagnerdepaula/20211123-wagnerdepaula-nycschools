//
//  SchoolViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

// I've created a simple enum to determine the order of cells and which high school data block to display

enum CellType {
    case header, contact, address, transportation, overview, satScores, activities
}


final class SchoolViewController: UIViewController {
    
    private lazy var schooItem: SchoolItem = SchoolItem()
    private lazy var satItem: SatItem = SatItem()
    private lazy var cellOrder: [CellType] = [.header, .contact, .address, .transportation, .overview, .satScores, .activities]
    
    public lazy var generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .light)
        return generator
    }()
    
    // It's always great to wrap my tableViews in a lazy var and declare whatever is necessary upfront, specially my cells.
    // it's more  organized this way.
    
    private lazy var tableView: UITableView = {
        let tableView: UITableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.isOpaque = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.delaysContentTouches = false
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SchoolHeaderCell.self, forCellReuseIdentifier: "SchoolHeaderCell")
        tableView.register(SchoolContactCell.self, forCellReuseIdentifier: "SchoolContactCell")
        tableView.register(SchoolAddressCell.self, forCellReuseIdentifier: "SchoolAddressCell")
        tableView.register(SchoolTransportationCell.self, forCellReuseIdentifier: "SchoolTransportationCell")
        tableView.register(SchoolOverviewCell.self, forCellReuseIdentifier: "SchoolOverviewCell")
        tableView.register(SchoolSatScoresCell.self, forCellReuseIdentifier: "SchoolSatScoresCell")
        tableView.register(SchoolActivitiesCell.self, forCellReuseIdentifier: "SchoolActivitiesCell")
        return tableView
    }()
    
    private lazy var swipeIndicator: UIView = {
        let swipeIndicator = UIView(frame: .zero)
        swipeIndicator.layer.cornerRadius = 3
        swipeIndicator.translatesAutoresizingMaskIntoConstraints = false
        swipeIndicator.backgroundColor = .darkGray
        return swipeIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        resetTableView()
    }
    
    private func getData() {
        self.schooItem = DataManager.shared.currentSchool
        
        // Get SAT Scores before reloading the UITableView
        // Ideally I could have requested them upfront along with all schools but it would have required more loading time upfront
        // However, it would insure we have all the data we need upfront wihtout worring about UITableView loading and reloading issues
        // Perhaps, this is an area for improvement
        
        DataManager.shared.getSatScores {
            self.satItem = DataManager.shared.currentSat
        }
    }
    
    
    // it's important to reset the tableView and variables when the view is no longer available
    // This avoids data inconsistencies
    
    private func resetTableView() {
        self.satItem = SatItem()
        self.schooItem = SchoolItem()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func setupView() {
        view.addSubview(tableView)
        view.addSubview(swipeIndicator)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            // swipeIndicator
            swipeIndicator.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            swipeIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            swipeIndicator.widthAnchor.constraint(equalToConstant: 40),
            swipeIndicator.heightAnchor.constraint(equalToConstant: 6),
            // tableView
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    
}


extension SchoolViewController: UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
            case 0:
                return 30
            default:
                return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return cellOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = getCell(for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func getCell(for indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell = UITableViewCell()
        let cellType: CellType = cellOrder[indexPath.section]
        
        switch cellType {
            case .header:
                guard let schoolHeaderCell = tableView.dequeueReusableCell(withIdentifier: "SchoolHeaderCell") as? SchoolHeaderCell else { return UITableViewCell() }
                schoolHeaderCell.config(with: schooItem)
                cell = schoolHeaderCell
            case .contact:
                guard let schoolContactCell = tableView.dequeueReusableCell(withIdentifier: "SchoolContactCell") as? SchoolContactCell else { return UITableViewCell() }
                schoolContactCell.config(with: schooItem)
                cell = schoolContactCell
            case .address:
                guard let schoolAddressCell = tableView.dequeueReusableCell(withIdentifier: "SchoolAddressCell") as? SchoolAddressCell else { return UITableViewCell() }
                schoolAddressCell.config(with: schooItem)
                cell = schoolAddressCell
            case .transportation:
                guard let schoolTransportationCell = tableView.dequeueReusableCell(withIdentifier: "SchoolTransportationCell") as? SchoolTransportationCell else { return UITableViewCell() }
                schoolTransportationCell.config(with: schooItem)
                cell = schoolTransportationCell
            case .overview:
                guard let schoolOverviewCell = tableView.dequeueReusableCell(withIdentifier: "SchoolOverviewCell") as? SchoolOverviewCell else { return UITableViewCell() }
                schoolOverviewCell.config(with: schooItem)
                cell = schoolOverviewCell
            case .satScores:
                guard let schoolSatScoresCell = tableView.dequeueReusableCell(withIdentifier: "SchoolSatScoresCell") as? SchoolSatScoresCell else { return UITableViewCell() }
                schoolSatScoresCell.config(with: satItem)
                cell = schoolSatScoresCell
            case .activities:
                guard let schoolActivitiesCell = tableView.dequeueReusableCell(withIdentifier: "SchoolActivitiesCell") as? SchoolActivitiesCell else { return UITableViewCell() }
                schoolActivitiesCell.config(with: schooItem)
                cell = schoolActivitiesCell
        }
        return cell
    }
}
