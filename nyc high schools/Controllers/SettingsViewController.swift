//
//  SettingsViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit


// Settings
// This is just a bonus to allow users to quickly switch between Dark and Light mode to test the UI
// Without having to jump into their iOS settings mode preferences and disrupting the experience.


public struct SettingsSection {
    var title: String = ""
    var items: [SettingsItem] = []
}

public struct SettingsItem {
    var name: String = ""
    var mode: Int = 2
    var icon: UIImage?
}


final class SettingsViewController: UIViewController {
    
    private lazy var sections: [SettingsSection] = {[
        SettingsSection(title: "Mode", items: [
            SettingsItem(name: "System", mode: 0, icon: UIImage(systemName: "slider.horizontal.3") ),
            SettingsItem(name: "Light", mode: 1, icon: UIImage(systemName: "sun.max.fill")),
            SettingsItem(name: "Dark", mode: 2, icon: UIImage(systemName: "moon.fill"))
        ])
    ]}()
    
    public lazy var generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .light)
        return generator
    }()
    
    private lazy var tableView: UITableView = {
        let tableView: UITableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.isOpaque = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.delaysContentTouches = false
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(SettingsCell.self, forCellReuseIdentifier: "SettingsCell")
        return tableView
    }()
    
    private lazy var swipeIndicator: UIView = {
        let swipeIndicator = UIView(frame: .zero)
        swipeIndicator.layer.cornerRadius = 3
        swipeIndicator.translatesAutoresizingMaskIntoConstraints = false
        swipeIndicator.backgroundColor = .darkGray
        return swipeIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setDefaultMode()
    }
    
    private func setupView() {
        view.addSubview(tableView)
        view.addSubview(swipeIndicator)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            // swipeIndicator
            swipeIndicator.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            swipeIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            swipeIndicator.widthAnchor.constraint(equalToConstant: 40),
            swipeIndicator.heightAnchor.constraint(equalToConstant: 6),
            // tableView
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func setDefaultMode() {
        let systemMode:Int  = ModeManager.shared.get().rawValue
        switchMode(indexPath: IndexPath(row: systemMode, section: 0))
    }
    
}



extension SettingsViewController: UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as? SettingsCell else { return UITableViewCell() }
        let section = sections[indexPath.section]
        let item = section.items[indexPath.row]
        cell.config(with: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        generator.impactOccurred()
        switchMode(indexPath: indexPath)
    }
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return  }
        cell.highlight()
    }
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.unHighlight()
    }
    
    
    private func switchMode(indexPath: IndexPath) {
        let section = sections[indexPath.section]
        let item = section.items[indexPath.row]
        resetAllCells(section: indexPath.section)
        checkCell(indexPath: indexPath)
        ModeManager.shared.set(mode: item.mode)
    }
    
    private func checkCell(indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.accessoryType = (cell.accessoryType == .checkmark) ? .none : .checkmark
    }
    
    private func resetAllCells(section: Int) {
        for row in 0..<tableView.numberOfRows(inSection: section) {
            if let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
                cell.accessoryType = .none
            }
        }
    }
    
}
