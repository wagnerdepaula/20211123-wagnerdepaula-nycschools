//
//  NavigationController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit
import MessageUI


// I have decided to do actionable features with the data provided by each school. Spcially contact info data.
// Users can call, email, navigate to website and share the school, if they decide too.
// Data should be consumed but they should be actionable, when they can be.


final class NavigationController: UINavigationController, MFMailComposeViewControllerDelegate {
    
    static var shared: NavigationController = NavigationController()
    public var settingsViewController: SettingsViewController = SettingsViewController()
    public var schoolViewController: SchoolViewController = SchoolViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationController.shared = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem()
        updateNavigation()
    }
    
    public func updateNavigation() {
        
        let title = [NSAttributedString.Key.font: roundedFont(ofSize: 20, weight: .bold),
                     NSAttributedString.Key.foregroundColor: UIColor.label]
        
        navigationBar.tintColor = .label
        navigationBar.backgroundColor = .systemBackground
        navigationBar.barTintColor = .systemBackground
        navigationBar.titleTextAttributes = title
        
        guard let tabBarController = tabBarController else { return }
        tabBarController.tabBar.tintColor = .label
        tabBarController.tabBar.barTintColor = .systemBackground
        tabBarController.tabBar.backgroundColor = .systemBackground
    }
    
    public func navigateToSettings() {
        DispatchQueue.main.async {
            self.settingsViewController.modalPresentationStyle = .automatic
            self.present(self.settingsViewController, animated: true, completion: nil)
        }
    }
    
    public func navigateToSchool() {
        DispatchQueue.main.async {
            self.schoolViewController.modalPresentationStyle = .automatic
            self.present(self.schoolViewController, animated: true, completion: nil)
        }
    }
    
    
    public func navigateToWebsite() {
        guard let website = DataManager.shared.currentSchool.website else { return }
        if let url = URL(string: "http://\(website)") {
            UIApplication.shared.open(url)
        }
    }
    
    public func sendEmail() {
        guard let email = DataManager.shared.currentSchool.email else { return }
        guard let schoolName = DataManager.shared.currentSchool.schoolName else { return }
        
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        if MFMailComposeViewController.canSendMail() {
            let topViewController = topMostViewController()
            let mailViewController = MFMailComposeViewController()
            mailViewController.mailComposeDelegate = self
            mailViewController.setToRecipients([email])
            mailViewController.setSubject(schoolName)
            topViewController.present(mailViewController, animated: true)
        }
        
    }
    
    public func callNumber() {
        guard let phoneNumber = DataManager.shared.currentSchool.phoneNumber else { return }
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application: UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    public func shareLocation() {
        
        guard let schoolName = DataManager.shared.currentSchool.schoolName else { return }
        let formatedSchoolName = schoolName.replacingOccurrences(of: " ", with: "+")
        guard let latitude = DataManager.shared.currentSchool.latitude else { return }
        guard let longitude = DataManager.shared.currentSchool.longitude else { return }
        
        if let url = URL(string: "https://maps.apple.com?q=\(formatedSchoolName)&ll=\(latitude),\(longitude)") {
            let topViewController = topMostViewController()
            let activityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            topViewController.present(activityViewController, animated: true)
        }
        
    }
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error {
            controller.dismiss(animated: true, completion: nil)
            return
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}



