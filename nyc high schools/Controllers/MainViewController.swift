//
//  MainViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

// The best way to handle view controllers in my opinion is just to setup all of them dinamically
// As dealing with storyboards can be tedious, intricate and expensive

final class MainViewController: UITabBarController {
    
    private let listViewController = ListViewController()
    private let mapViewController = MapViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        setupView()
    }
    
    private func setupView() {
        
        let large = UIImage.SymbolConfiguration(scale: .large)
        let controllers = [NavigationController(rootViewController: listViewController),
                           NavigationController(rootViewController: mapViewController)]
        
        listViewController.tabBarItem.title = "List"
        listViewController.tabBarItem.image = UIImage(systemName: "list.bullet", withConfiguration: large)
        listViewController.tabBarItem.selectedImage = UIImage(systemName: "list.bullet.fill", withConfiguration: large)
        
        mapViewController.tabBarItem.title = "Map"
        mapViewController.tabBarItem.image = UIImage(systemName: "map", withConfiguration: large)
        mapViewController.tabBarItem.selectedImage = UIImage(systemName: "map.fill", withConfiguration: large)

        viewControllers = controllers
    }
    
}


extension MainViewController: UITabBarControllerDelegate {
    
    
    
}
