//
//  ListViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit

// This ListViewController
// I like to declare lazy vars in order to minimize memory overload as they never have to be calculated or loaded more than once
// Also, final classes are important here as I'm not modifying them at this point so they don't usually get shared in the vtable.
// Another good perfomance trick.


final class ListViewController: UIViewController {
    
    public var schoolItems: [SchoolItem] = []
    
    // A subtle impact feedback is always good and gives a more intentional experience
    public lazy var generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .light)
        return generator
    }()
    
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        return activityIndicatorView
    }()
    
    private lazy var settingsButton: UIBarButtonItem = {
        let image = UIImage(systemName: "slider.horizontal.3", withConfiguration: UIImage.SymbolConfiguration(scale: .large))
        let settingsButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(navigateToSettings))
        return settingsButton
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.isOpaque = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = .systemBackground
        tableView.delaysContentTouches = false
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(ListCell.self, forCellReuseIdentifier: "ListCell")
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateNavigation()
        setupActivityIndicator()
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    private func getData() {
        DataManager.shared.getSchools {
            self.schoolItems = DataManager.shared.schoolItems
            self.tableView.reloadData()
            self.activityIndicatorView.stopAnimating()
        }
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView.frame = view.frame
        activityIndicatorView.center = view.center
        activityIndicatorView.startAnimating()
    }
    
    private func setupView() {
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        view.addSubview(activityIndicatorView)
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func updateNavigation() {
        navigationItem.title = "List"
        navigationItem.rightBarButtonItem = settingsButton
    }
    
    @objc private func navigateToSettings() {
        NavigationController.shared.navigateToSettings()
    }
    
}



extension ListViewController: UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate {
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let listCell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as? ListCell else { return UITableViewCell() }
        let schoolItem: SchoolItem = schoolItems[indexPath.row]
        listCell.config(with: schoolItem)
        return listCell
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolItems.count
    }
    
    // It's Always good to have cells automatically resize themselves based on content
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        generator.impactOccurred()
        let schoolItem: SchoolItem = schoolItems[indexPath.row]
        setCurrentSchool(with: schoolItem)
    }
    
    public func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.highlight()
    }
    
    public func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        cell.unHighlight()
    }
    
    private func setCurrentSchool(with schoolItem: SchoolItem) {
        DataManager.shared.setCurrentSchool(with: schoolItem)
        NavigationController.shared.navigateToSchool()
    }
    
}


