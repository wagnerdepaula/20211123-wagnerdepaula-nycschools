//
//  UIFont.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit

// I liked to use system fonts but more specifically, the rounded ones as they have a more elegant appearance

public func roundedFont(ofSize fontSize: CGFloat, weight: UIFont.Weight) -> UIFont {
    if let descriptor = UIFont.systemFont(ofSize: fontSize, weight: weight).fontDescriptor.withDesign(.rounded) {
        return UIFont(descriptor: descriptor, size: fontSize)
    } else {
        return UIFont.systemFont(ofSize: fontSize, weight: weight)
    }
}
