//
//  UIViewController.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/23/21.
//

import UIKit

// As I have to dynamically manipulate ViewControllers that are usually wrapped in UINagivationControllers or UITabBarViewController
// It's usually good to have a way to easly access the TopMostViewController globally.

extension UIViewController {
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}
