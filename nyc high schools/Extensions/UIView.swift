//
//  UIView.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit

// Usually good to apply my own cell selection effects.
// The effects provided by UIKit are too simplistic and this way I have more control over what the cells looks like

extension UIView {
    
    public func highlight() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.allowUserInteraction], animations: {
            self.alpha = 0.6
        })
    }
    
    public func unHighlight() {
        UIView.animate(withDuration: 0.4, delay: 0, options: [.allowUserInteraction], animations: {
            self.alpha = 1
        })
    }
    
}
