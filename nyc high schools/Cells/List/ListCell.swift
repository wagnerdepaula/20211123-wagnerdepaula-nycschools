//
//  ListCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/21/21.
//

import UIKit

final class ListCell: UITableViewCell {
    
    private var schoolItem: SchoolItem = SchoolItem()
    
    private lazy var iconView: UIView = {
        let iconView = UIView(frame: .zero)
        iconView.backgroundColor = .secondarySystemBackground
        iconView.layer.cornerRadius = 35
        iconView.translatesAutoresizingMaskIntoConstraints = false
        return iconView
    }()
    
    private lazy var iconLabel: UILabel = {
        let iconLabel = UILabel(frame: .zero)
        iconLabel.isOpaque = true
        iconLabel.font = roundedFont(ofSize: 32, weight: .medium)
        iconLabel.numberOfLines = 0
        iconLabel.textAlignment = .center
        iconLabel.translatesAutoresizingMaskIntoConstraints = false
        return iconLabel
    }()
    
    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel(frame: .zero)
        nameLabel.isOpaque = true
        nameLabel.font = roundedFont(ofSize: 18, weight: .medium)
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .left
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    private lazy var iconGrade: UIImageView = {
        let iconGrade: UIImageView = UIImageView(frame: .zero)
        let iconImage = UIImage(systemName: "graduationcap.fill", withConfiguration: UIImage.SymbolConfiguration(scale: .medium))
        iconGrade.image = iconImage
        iconGrade.tintColor = .systemOrange
        iconGrade.translatesAutoresizingMaskIntoConstraints = false
        return iconGrade
    }()
    
    private lazy var gradesLabel: UILabel = {
        let gradesLabel = UILabel(frame: .zero)
        gradesLabel.isOpaque = true
        gradesLabel.font = roundedFont(ofSize: 16, weight: .regular)
        gradesLabel.textColor = .lightGray
        gradesLabel.numberOfLines = 0
        gradesLabel.textAlignment = .left
        gradesLabel.translatesAutoresizingMaskIntoConstraints = false
        return gradesLabel
    }()
    
    public func config(with schoolItem: SchoolItem) {
        self.schoolItem = schoolItem
        setupView()
        setupLayout()
        updateViews()
    }
    
    private func setupView() {
        isOpaque = true
        separatorInset = UIEdgeInsets(top: 0, left: 100, bottom: 0, right: 0)
        selectionStyle = .none
        backgroundColor = .clear
        
        iconView.addSubview(iconLabel)
        contentView.addSubview(iconView)
        contentView.addSubview(iconGrade)
        contentView.addSubview(nameLabel)
        contentView.addSubview(gradesLabel)
    }
    
    private func updateViews() {
        iconLabel.text = "🏫"
        nameLabel.text = schoolItem.schoolName
        
        // I usually handle these type of data treatment under DataModel
        // Perhaps an area to improve here.
        
        guard let finalGrades: String = schoolItem.finalGrades else { return }
        gradesLabel.text = finalGrades.replacingOccurrences(of: "-", with: " to ")
    }
    
    // Aside from using StackViews, this is the best way to handle auto layouts, in my opinion.
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.widthAnchor.constraint(equalToConstant: 70),
            iconView.heightAnchor.constraint(equalToConstant: 70),
            
            iconLabel.centerXAnchor.constraint(equalTo: iconView.centerXAnchor),
            iconLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor, constant: -5),
            
            nameLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 15),
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            iconGrade.widthAnchor.constraint(equalToConstant: 20),
            iconGrade.heightAnchor.constraint(equalToConstant: 20),
            iconGrade.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            iconGrade.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 15),
            iconGrade.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
            
            gradesLabel.leadingAnchor.constraint(equalTo: iconGrade.trailingAnchor, constant: 10),
            gradesLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
            gradesLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            gradesLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        ])
    }
}
