//
//  SettingsCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

final class SettingsCell: UITableViewCell {
    
    private var item: SettingsItem = SettingsItem()
    
    private lazy var iconView: UIImageView = {
        let iconView = UIImageView(frame: .zero)
        iconView.isOpaque = true
        iconView.translatesAutoresizingMaskIntoConstraints = false
        return iconView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.font = roundedFont(ofSize: 16, weight: .regular)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.adjustsFontForContentSizeCategory = true
        return titleLabel
    }()
    
    public func config(with item: SettingsItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = UIEdgeInsets(top: 0, left: 60, bottom: 0, right: 0)
        tintColor = .label
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
    }
    
    private func updateViews() {
        titleLabel.text = item.name
        iconView.image = item.icon
        iconView.tintColor = .label
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // iconView
            iconView.widthAnchor.constraint(equalToConstant: 20),
            iconView.heightAnchor.constraint(equalToConstant: 20),
            iconView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            
            // titleLabel
            titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 20),
            titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
}
