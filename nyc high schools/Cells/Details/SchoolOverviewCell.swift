//
//  SchoolOverviewCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

final class SchoolOverviewCell: UITableViewCell {
    
    private var item: SchoolItem = SchoolItem()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .label
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var bodyLabel: UILabel = {
        let bodyLabel = UILabel(frame: .zero)
        bodyLabel.isOpaque = true
        bodyLabel.font = roundedFont(ofSize: 16, weight: .regular)
        bodyLabel.textColor = .secondaryLabel
        bodyLabel.numberOfLines = 0
        bodyLabel.textAlignment = .left
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        return bodyLabel
    }()
    
    public func config(with item: SchoolItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        contentView.addSubview(titleLabel)
        contentView.addSubview(bodyLabel)
    }
    
    private func updateViews() {
        titleLabel.text = "Overview"
        bodyLabel.text = item.overviewParagraph
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // titleLabel
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // bodyLabel
            bodyLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            bodyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            bodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            bodyLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
            
        ])
    }
    
}
