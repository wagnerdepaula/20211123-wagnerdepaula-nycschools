//
//  SchoolSatScoresCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/23/21.
//

import UIKit

final class SchoolSatScoresCell: UITableViewCell {
    
    private var item: SatItem = SatItem()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .label
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var mainStackView: UIStackView = {
        let mainStackView = UIStackView(frame: .zero)
        mainStackView.isOpaque = true
        mainStackView.axis = .horizontal
        mainStackView.alignment = .center
        mainStackView.spacing = 15
        mainStackView.distribution = .fill
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        return mainStackView
    }()
    
    public func config(with item: SatItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
        
        setupStackView(icon: "x.squareroot", score: item.mathScore, title: "Math")
        setupStackView(icon: "eyeglasses", score: item.readingScore, title: "Reading")
        setupStackView(icon: "pencil", score: item.writingScore, title: "Writing")
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        contentView.addSubview(titleLabel)
        contentView.addSubview(mainStackView)
    }
    
    private func updateViews() {
        titleLabel.text = "SAT Scores"
    }
    
    private func setupStackView(icon: String, score:String?, title: String) {
        
        let verticalStackView = UIStackView(frame: .zero)
        verticalStackView.isOpaque = true
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .center
        verticalStackView.spacing = 5
        
        let iconImage = UIImage(systemName: icon, withConfiguration: UIImage.SymbolConfiguration(scale: .large))
        let iconView = UIImageView(frame: .zero)
        iconView.isOpaque = true
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.tintColor = .systemOrange
        iconView.contentMode = .scaleAspectFit
        iconView.image = iconImage
        
        let scoreLabel = UILabel(frame: .zero)
        scoreLabel.isOpaque = true
        scoreLabel.font = roundedFont(ofSize: 28, weight: .regular)
        scoreLabel.textColor = .label
        scoreLabel.numberOfLines = 0
        scoreLabel.textAlignment = .center
        scoreLabel.text = score ?? " - "
        
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.font = roundedFont(ofSize: 12, weight: .medium)
        titleLabel.textColor = .secondaryLabel
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.text = title
        
        verticalStackView.addArrangedSubview(iconView)
        verticalStackView.addArrangedSubview(scoreLabel)
        verticalStackView.addArrangedSubview(titleLabel)
        mainStackView.addArrangedSubview(verticalStackView)
        
    }
    
    private func resetStackView() {
        for view in mainStackView.subviews {
            view.removeFromSuperview()
        }
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // titleLabel
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // mainStackView
            mainStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ])
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resetStackView()
    }
    
}
