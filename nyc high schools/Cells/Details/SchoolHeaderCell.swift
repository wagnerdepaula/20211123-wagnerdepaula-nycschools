//
//  SchoolHeader.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

final class SchoolHeaderCell: UITableViewCell {
    
    private var item: SchoolItem = SchoolItem()
    
    private lazy var horizontalStackView: UIStackView = {
        let horizontalStackView = UIStackView(frame: .zero)
        horizontalStackView.isOpaque = true
        horizontalStackView.axis = .horizontal
        horizontalStackView.alignment = .leading
        horizontalStackView.spacing = 5
        horizontalStackView.distribution = .fill
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        return horizontalStackView
    }()
    
    private lazy var iconLabel: UILabel = {
        let iconLabel = UILabel(frame: .zero)
        iconLabel.isOpaque = true
        iconLabel.font = roundedFont(ofSize: 64, weight: .medium)
        iconLabel.numberOfLines = 0
        iconLabel.textAlignment = .center
        iconLabel.translatesAutoresizingMaskIntoConstraints = false
        return iconLabel
    }()
    
    private lazy var nameLabel: UILabel = {
        let nameLabel = UILabel(frame: .zero)
        nameLabel.isOpaque = true
        nameLabel.font = roundedFont(ofSize: 24, weight: .medium)
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .center
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    public func config(with item: SchoolItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
        
        // Total Students
        setupStackView(icon: "person.3.sequence.fill", title: item.totalStudents)
        
        // Final Grades
        guard let finalGrades: String = item.finalGrades else { return }
        let grades = finalGrades.replacingOccurrences(of: "-", with: " to ")
        setupStackView(icon: "graduationcap.fill", title: grades)
        
        // Attendance Rate
        guard let attendanceRate: String = item.attendanceRate else { return }
        let attendancePecentage = (Double(attendanceRate) ?? 0) * 100
        let attendance = String(format: "%.1f%%", attendancePecentage)
        setupStackView(icon: "hand.raised.fill", title: attendance)
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        contentView.addSubview(iconLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(horizontalStackView)
    }
    
    private func updateViews() {
        iconLabel.text = "🏫"
        nameLabel.text = item.schoolName
    }
    
    private func setupStackView(icon: String, title: String?) {
        
        let iconImage = UIImage(systemName: icon, withConfiguration: UIImage.SymbolConfiguration(scale: .small))
        let iconView = UIImageView(frame: .zero)
        iconView.isOpaque = true
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.tintColor = .systemOrange
        iconView.image = iconImage
        
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.font = roundedFont(ofSize: 16, weight: .regular)
        titleLabel.textColor = .label
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center
        titleLabel.text = title
        
        horizontalStackView.addArrangedSubview(iconView)
        horizontalStackView.addArrangedSubview(titleLabel)
        horizontalStackView.setCustomSpacing(20, after: titleLabel)
    }
    
    
    private func resetStackView() {
        for view in horizontalStackView.subviews {
            view.removeFromSuperview()
        }
    }
    
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // iconLabel
            iconLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 40),
            iconLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            iconLabel.widthAnchor.constraint(equalToConstant: 80),
            iconLabel.heightAnchor.constraint(equalToConstant: 80),
            
            // nameLabel
            nameLabel.topAnchor.constraint(equalTo: iconLabel.bottomAnchor, constant: 10),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            // horizontalStackView
            horizontalStackView.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 40),
            horizontalStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            horizontalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -40),
            horizontalStackView.widthAnchor.constraint(lessThanOrEqualToConstant: 300),
        ])
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resetStackView()
    }
    
}
