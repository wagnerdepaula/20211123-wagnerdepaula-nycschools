//
//  SchoolContactCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

enum ContactType {
    case phone
    case email
    case website
    case share
}

final class SchoolContactCell: UITableViewCell {
    
    private var item: SchoolItem = SchoolItem()
    
    public lazy var generator: UIImpactFeedbackGenerator = {
        let generator = UIImpactFeedbackGenerator(style: .light)
        return generator
    }()
    
    private lazy var horizontalStackView: UIStackView = {
        let horizontalStackView = UIStackView(frame: .zero)
        horizontalStackView.isOpaque = true
        horizontalStackView.axis = .horizontal
        horizontalStackView.alignment = .leading
        horizontalStackView.spacing = 15
        horizontalStackView.distribution = .fill
        horizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        return horizontalStackView
    }()
    
    public func config(with item: SchoolItem) {
        self.item = item
        setupView()
        setupLayout()
        
        setupStackView(icon: "phone.fill", title: "Call", type: .phone)
        setupStackView(icon: "envelope.fill", title: "Email", type: .email)
        setupStackView(icon: "safari.fill", title: "Website", type: .website)
        setupStackView(icon: "square.and.arrow.up", title: "Share", type: .share)
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        contentView.addSubview(horizontalStackView)
    }
    
    
    
    private func setupStackView(icon: String, title: String, type: ContactType) {
        
        let verticalStackView = UIStackView(frame: .zero)
        verticalStackView.isOpaque = true
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .center
        verticalStackView.spacing = 10
        
        let iconImage = UIImage(systemName: icon, withConfiguration: UIImage.SymbolConfiguration(scale: .large))
        let iconView = UIImageView(frame: .zero)
        iconView.isOpaque = true
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.tintColor = .systemBlue
        iconView.image = iconImage
        
        let titleLabel = UILabel(frame: .zero)
        titleLabel.isOpaque = true
        titleLabel.font = roundedFont(ofSize: 12, weight: .medium)
        titleLabel.textColor = .systemBlue
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.text = title
        
        verticalStackView.addArrangedSubview(iconView)
        verticalStackView.addArrangedSubview(titleLabel)
        horizontalStackView.addArrangedSubview(verticalStackView)
        
        switch type {
            case .phone:
                verticalStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(callNumber)))
            case .email:
                verticalStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendEmail)))
            case .website:
                verticalStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigateToWebsite)))
            case .share:
                verticalStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shareLocation)))
        }
        
    }
    
    @objc private func callNumber(gesture : UITapGestureRecognizer) {
        handleTapEffect(gesture: gesture)
        NavigationController.shared.callNumber()
    }
    
    @objc private func sendEmail(gesture : UITapGestureRecognizer) {
        handleTapEffect(gesture: gesture)
        NavigationController.shared.sendEmail()
    }
    
    @objc private func navigateToWebsite(gesture : UITapGestureRecognizer) {
        handleTapEffect(gesture: gesture)
        NavigationController.shared.navigateToWebsite()
    }
    
    @objc private func shareLocation(gesture : UITapGestureRecognizer) {
        handleTapEffect(gesture: gesture)
        NavigationController.shared.shareLocation()
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            horizontalStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            horizontalStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            horizontalStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            horizontalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)
        ])
    }
    
    private func resetStackView() {
        for view in horizontalStackView.subviews {
            view.removeFromSuperview()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        resetStackView()
    }
    
    private func handleTapEffect(gesture: UITapGestureRecognizer) {
        guard let view = gesture.view else { return }
        
        view.highlight()
        
        if gesture.state == .ended {
            view.unHighlight()
        }
        
        generator.impactOccurred()
    }
    
}
