//
//  SchoolActivitiesCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

final class SchoolActivitiesCell: UITableViewCell {
    
    private var item: SchoolItem = SchoolItem()
    
    private lazy var activitiesTitleLabel: UILabel = {
        let activitiesTitleLabel = UILabel(frame: .zero)
        activitiesTitleLabel.isOpaque = true
        activitiesTitleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        activitiesTitleLabel.numberOfLines = 0
        activitiesTitleLabel.textAlignment = .left
        activitiesTitleLabel.textColor = .label
        activitiesTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return activitiesTitleLabel
    }()
    
    private lazy var activitiesBodyLabel: UILabel = {
        let activitiesBodyLabel = UILabel(frame: .zero)
        activitiesBodyLabel.isOpaque = true
        activitiesBodyLabel.font = roundedFont(ofSize: 16, weight: .regular)
        activitiesBodyLabel.textColor = .secondaryLabel
        activitiesBodyLabel.numberOfLines = 0
        activitiesBodyLabel.textAlignment = .left
        activitiesBodyLabel.translatesAutoresizingMaskIntoConstraints = false
        return activitiesBodyLabel
    }()
    
    private lazy var sportsTitleLabel: UILabel = {
        let sportsTitleLabel = UILabel(frame: .zero)
        sportsTitleLabel.isOpaque = true
        sportsTitleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        sportsTitleLabel.numberOfLines = 0
        sportsTitleLabel.textAlignment = .left
        sportsTitleLabel.textColor = .label
        sportsTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return sportsTitleLabel
    }()
    
    private lazy var sportsBodyLabel: UILabel = {
        let sportsBodyLabel = UILabel(frame: .zero)
        sportsBodyLabel.isOpaque = true
        sportsBodyLabel.font = roundedFont(ofSize: 16, weight: .regular)
        sportsBodyLabel.textColor = .secondaryLabel
        sportsBodyLabel.numberOfLines = 0
        sportsBodyLabel.textAlignment = .left
        sportsBodyLabel.translatesAutoresizingMaskIntoConstraints = false
        return sportsBodyLabel
    }()
    
    public func config(with item: SchoolItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        
        contentView.addSubview(activitiesTitleLabel)
        contentView.addSubview(activitiesBodyLabel)
        contentView.addSubview(sportsTitleLabel)
        contentView.addSubview(sportsBodyLabel)
    }
    
    private func updateViews() {
        activitiesTitleLabel.text = "Activities"
        activitiesBodyLabel.text = item.extracurricularActivities
        sportsTitleLabel.text = "Sports"
        sportsBodyLabel.text = item.schoolSports
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // activitiesTitleLabel
            activitiesTitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            activitiesTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            activitiesTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // activitiesBodyLabel
            activitiesBodyLabel.topAnchor.constraint(equalTo: activitiesTitleLabel.bottomAnchor, constant: 5),
            activitiesBodyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            activitiesBodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // sportsTitleLabel
            sportsTitleLabel.topAnchor.constraint(equalTo: activitiesBodyLabel.bottomAnchor, constant: 15),
            sportsTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            sportsTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // sportsBodyLabel
            sportsBodyLabel.topAnchor.constraint(equalTo: sportsTitleLabel.bottomAnchor, constant: 5),
            sportsBodyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            sportsBodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            sportsBodyLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
            
        ])
    }
    
}
