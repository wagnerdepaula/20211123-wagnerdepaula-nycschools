//
//  SchoolTransportationCell.swift
//  nyc high schools
//
//  Created by Wagner De Paula on 11/22/21.
//

import UIKit

final class SchoolTransportationCell: UITableViewCell {
    
    private var item: SchoolItem = SchoolItem()
    
    private lazy var subwayTitleLabel: UILabel = {
        let subwayTitleLabel = UILabel(frame: .zero)
        subwayTitleLabel.isOpaque = true
        subwayTitleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        subwayTitleLabel.numberOfLines = 0
        subwayTitleLabel.textAlignment = .left
        subwayTitleLabel.textColor = .label
        subwayTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return subwayTitleLabel
    }()
    
    private lazy var subwayBodyLabel: UILabel = {
        let subwayBodyLabel = UILabel(frame: .zero)
        subwayBodyLabel.isOpaque = true
        subwayBodyLabel.font = roundedFont(ofSize: 16, weight: .regular)
        subwayBodyLabel.textColor = .secondaryLabel
        subwayBodyLabel.numberOfLines = 0
        subwayBodyLabel.textAlignment = .left
        subwayBodyLabel.translatesAutoresizingMaskIntoConstraints = false
        return subwayBodyLabel
    }()
    
    private lazy var busTitleLabel: UILabel = {
        let busTitleLabel = UILabel(frame: .zero)
        busTitleLabel.isOpaque = true
        busTitleLabel.font = roundedFont(ofSize: 14, weight: .medium)
        busTitleLabel.numberOfLines = 0
        busTitleLabel.textAlignment = .left
        busTitleLabel.textColor = .label
        busTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return busTitleLabel
    }()
    
    private lazy var busBodyLabel: UILabel = {
        let busBodyLabel = UILabel(frame: .zero)
        busBodyLabel.isOpaque = true
        busBodyLabel.font = roundedFont(ofSize: 16, weight: .regular)
        busBodyLabel.textColor = .secondaryLabel
        busBodyLabel.numberOfLines = 0
        busBodyLabel.textAlignment = .left
        busBodyLabel.translatesAutoresizingMaskIntoConstraints = false
        return busBodyLabel
    }()
    
    public func config(with item: SchoolItem) {
        self.item = item
        setupView()
        setupLayout()
        updateViews()
    }
    
    private func setupView() {
        isOpaque = true
        selectionStyle = .none
        separatorInset = .zero
        tintColor = .label
        
        contentView.addSubview(subwayTitleLabel)
        contentView.addSubview(subwayBodyLabel)
        contentView.addSubview(busTitleLabel)
        contentView.addSubview(busBodyLabel)
    }
    
    private func updateViews() {
        subwayTitleLabel.text = "Subway"
        subwayBodyLabel.text = item.subway
        busTitleLabel.text = "Bus"
        busBodyLabel.text = item.bus
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate([
            
            // subwayTitleLabel
            subwayTitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            subwayTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            subwayTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // subwayBodyLabel
            subwayBodyLabel.topAnchor.constraint(equalTo: subwayTitleLabel.bottomAnchor, constant: 5),
            subwayBodyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            subwayBodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // busTitleLabel
            busTitleLabel.topAnchor.constraint(equalTo: subwayBodyLabel.bottomAnchor, constant: 15),
            busTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            busTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            
            // busBodyLabel
            busBodyLabel.topAnchor.constraint(equalTo: busTitleLabel.bottomAnchor, constant: 5),
            busBodyLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            busBodyLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            busBodyLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15)
            
        ])
    }
    
}
